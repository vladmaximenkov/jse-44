package ru.vmaksimenkov.tm.exception.system;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Argument not found...");
    }

}

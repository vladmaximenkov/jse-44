package ru.vmaksimenkov.tm.exception.entity;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class ComparatorNotFoundException extends AbstractException {

    public ComparatorNotFoundException() {
        super("Error! Comparator not found...");
    }

}

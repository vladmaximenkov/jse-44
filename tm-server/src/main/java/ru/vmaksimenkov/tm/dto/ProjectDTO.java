package ru.vmaksimenkov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.entity.IWBS;
import ru.vmaksimenkov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "app_project")
@NoArgsConstructor
public class ProjectDTO extends AbstractBusinessEntityDTO implements IWBS {

    @Column
    @Nullable
    private Date created = new Date();

    @Nullable
    @Column(name = "date_finish")
    private Date dateFinish;

    @Nullable
    @Column(name = "date_start")
    private Date dateStart;

    @Column
    @Nullable
    private String description;

    @Column
    @Nullable
    private String name;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    public ProjectDTO(@Nullable final String name) {
        this.name = name;
    }

    public void setStatus(@Nullable final Status status) {
        this.status = status;
        if (status == null) return;
        switch (status) {
            case IN_PROGRESS:
                this.setDateStart(new Date());
                break;
            case COMPLETE:
                this.setDateFinish(new Date());
            default:
                break;
        }
    }

    @NotNull
    @Override
    public String toString() {
        return String.format("| %s | %-12s | %-20s | %-30s | %-30s | %-30s ", getId(), status, name, created, dateStart, dateFinish);
    }

}

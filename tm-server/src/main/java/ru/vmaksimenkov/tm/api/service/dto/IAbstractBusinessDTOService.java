package ru.vmaksimenkov.tm.api.service.dto;

import ru.vmaksimenkov.tm.api.repository.dto.IAbstractBusinessDTORepository;
import ru.vmaksimenkov.tm.dto.AbstractBusinessEntityDTO;

public interface IAbstractBusinessDTOService<E extends AbstractBusinessEntityDTO> extends IAbstractBusinessDTORepository<E> {

}

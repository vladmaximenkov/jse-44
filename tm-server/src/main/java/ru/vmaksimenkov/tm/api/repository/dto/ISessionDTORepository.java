package ru.vmaksimenkov.tm.api.repository.dto;

import ru.vmaksimenkov.tm.dto.SessionDTO;

public interface ISessionDTORepository extends IAbstractBusinessDTORepository<SessionDTO> {

}
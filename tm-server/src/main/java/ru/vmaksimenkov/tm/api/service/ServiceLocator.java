package ru.vmaksimenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.service.dto.*;
import ru.vmaksimenkov.tm.api.service.model.IProjectTaskService;
import ru.vmaksimenkov.tm.api.service.model.IUserService;

public interface ServiceLocator {

    @NotNull IAuthService getAuthService();

    @NotNull IConnectionService getConnectionService();

    @NotNull IProjectDTOService getProjectService();

    @NotNull IProjectTaskDTOService getProjectTaskService();

    @NotNull IProjectTaskService getProjectTaskServiceGraph();

    @NotNull IPropertyService getPropertyService();

    @NotNull ISessionDTOService getSessionService();

    @NotNull ITaskDTOService getTaskService();

    @NotNull IUserDTOService getUserService();

    @NotNull IUserService getUserServiceGraph();

}

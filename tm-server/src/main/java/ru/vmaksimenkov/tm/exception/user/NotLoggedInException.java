package ru.vmaksimenkov.tm.exception.user;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class NotLoggedInException extends AbstractException {

    public NotLoggedInException() {
        super("Error! You are not logged in...");
    }

}

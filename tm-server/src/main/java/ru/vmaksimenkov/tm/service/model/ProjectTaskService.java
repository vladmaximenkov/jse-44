package ru.vmaksimenkov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.model.IProjectRepository;
import ru.vmaksimenkov.tm.api.repository.model.ITaskRepository;
import ru.vmaksimenkov.tm.api.service.IConnectionService;
import ru.vmaksimenkov.tm.api.service.model.IProjectTaskService;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.model.Task;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.repository.model.ProjectRepository;
import ru.vmaksimenkov.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    @SneakyThrows
    public void bindTaskByProjectId(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(em);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(em);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            taskRepository.bindTaskPyProjectId(userId, projectId, taskId);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void clearTasks(@NotNull final String userId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(em);
            @Nullable final List<Project> list = projectRepository.findAll(userId);
            for (Project p : list) {
                em.remove(p);
            }
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(em);
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(em);
            em.remove(projectRepository.findById(userId, projectId));
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @NotNull final Integer projectIndex) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(em);
            if (!checkIndex(projectIndex, projectRepository.size(userId))) throw new IndexIncorrectException();
            em.remove(projectRepository.findByIndex(userId, projectIndex));
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String projectName) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(em);
            em.remove(projectRepository.findByName(userId, projectName));
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@NotNull final String userId, @Nullable final String taskId) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(em);
            taskRepository.unbindTaskFromProject(userId, taskId);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

}
